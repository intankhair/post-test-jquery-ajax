$(document).ready(function () {
  console.log("jquery ready");
  
  const loginInfo = [];

  // regex password
  const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

  // get element input with id
  const inputUsername = $("#username");
  const inputPassword = $('#password');

  // get form by id
  const loginForm = $("#login-form");

  // bolean for hide and show password
  let isShow = false;

  // button hiden and show password
  const buttonHideShow = $('#icon-click');

  const result = $("#result-container");
  const formContainer = $("#form-container");

  const unameMessage = $("#username-message");
  const passMessage = $("#password-message");

  buttonHideShow.on('click', handleHideShow);
  inputPassword.on('keyup', handlePasswordStrength);

  loginForm.on("submit", function (e) {
    e.preventDefault();
    // console.log("submit login button");

    inputValidation();
  });

  function inputValidation() {

    const usernameValue = inputUsername.val();
    const passwordValue = inputPassword.val();

    if (usernameValue === "") {
      unameMessage.text(`Username can't be blank`);
      unameMessage.css({
        "font-size": "14px",
        display: "block",
        color: "red",
      });
    }

    if (passwordValue === "") {
      passMessage.text(`Password can't be blank`);
      passMessage.css({
        "font-size": "14px",
        display: "block",
        color: "red",
        "margin-bottom": "2rem"
      });
    } else if (passwordValue.length < 8) {
      console.log(passwordValue.length)
      passMessage.text(`Password at least 8 characters`);
      passMessage.css({
        "font-size": "14px",
        display: "block",
        color: "red",
        "margin-bottom": "2rem"
      });
    }

    if(usernameValue != "" && passMessage != "" && passwordValue.length >= 8){
      console.log("kondisi bisa login");
      handleLogin();
    }
  }

  function handlePasswordStrength() {
    const value = $(this).val();
    if (strongRegex.test(value)) {
      console.log("password strong");
      passMessage.text(`Password strong`);
      passMessage.css({
        "font-size": "14px",
        display: "block",
        color: "green",
        "margin-bottom": "2rem"
      });
    } else if (mediumRegex.test(value)) {
      console.log("password medium");
      passMessage.text(`Password medium`);
      passMessage.css({
        "font-size": "14px",
        display: "block",
        color: "green",
        "margin-bottom": "2rem"
      });
    } else {
      console.log("password weak");
      passMessage.text(`Password weak`);
      passMessage.css({
        "font-size": "14px",
        display: "block",
        color: "red",
        "margin-bottom": "2rem"
      });
    }
  }

  function handleHideShow(e) {
    if (isShow) {
      console.log("is show");
      inputPassword.attr('type', 'text');
      isShow = false;
      $("#hide-show-icon").removeClass("fa-eye");
      $("#hide-show-icon").addClass("fa-eye-slash");
    } else {
      console.log("is hide");
      inputPassword.attr('type', 'password');
      isShow = true;
      $("#hide-show-icon").removeClass("fa-eye-slash");
      $("#hide-show-icon").addClass("fa-eye");
    }
  }

  function handleLogin(){

    const payload = {
      uname: inputUsername.val(),
      pass: inputPassword.val(),
    };

    console.log(payload);
    loginInfo.push(payload);
    renderItem();
  }

  function renderItem() {
    formContainer.css("display", "none");
    result.css("display", "block");
    const itemHTML = loginInfo.map(item => {
      return `
        <p>Hi ${item.uname}, Welcome!</>
      `
    });

    result.html(itemHTML.join(" "));
  }
});
